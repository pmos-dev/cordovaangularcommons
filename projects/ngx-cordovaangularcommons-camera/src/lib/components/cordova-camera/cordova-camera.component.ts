import { Component, OnInit } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { CommonsSnackService } from 'ngx-webappcommons-mobile';
import { CommonsDrawerService } from 'ngx-webappcommons-mobile';
import { ECommonsDrawerDirection } from 'ngx-webappcommons-mobile';

// Cordova features not present in the generic JS namespace
declare var Camera: any;
// tslint:disable-next-line:no-namespace
declare namespace navigator {
		let camera: any;
		let mediaDevices: any;
}

@Component({
		selector: 'cordova-camera',
		templateUrl: './cordova-camera.component.html',
		styleUrls: ['./cordova-camera.component.less']
})
export class CordovaCameraComponent extends CommonsComponent implements OnInit {
	ECommonsDrawerDirection = ECommonsDrawerDirection;
	
	@Input() autoPhoto: boolean = false;
	@Output() onSave: EventEmitter<string> = new EventEmitter<string>(true);
	
	photo: string|undefined;

	constructor(
			private drawerService: CommonsDrawerService,
			private snackService: CommonsSnackService
	) {
		super();
	}
	
	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.drawerService.showObservable(),
				(name: string): void => {
					if (name === 'cordova-camera') {
						this.photo = undefined;
						
						if (this.autoPhoto) this.doPhoto();
					}
				}
		);
		
		this.subscribe(
				this.drawerService.hideObservable(),
				(name: string): void => {
					if (name === 'cordova-camera') {
						this.photo = undefined;
					}
				}
		);
	}
	
	getBackgroundImage(): string|undefined {
		if (!this.photo) return undefined;
		return `url(${this.photo})`;
	}
	
	doPhoto(): void {
		navigator.camera.getPicture(
				(data: string): void => {
					this.photo = `data:image/jpeg;base64,${data}`;
				},
				(error: string): void => {
					this.snackService.error(error);
				},
				{
						quality: 90,
						targetWidth: 480,
						targetHeight: 360,
						destinationType: Camera.DestinationType.DATA_URL,
						correctOrientation: true
				}
		);
	}
	
	doCancel(): void {
		this.drawerService.hide('cordova-camera');
	}
	
	doSave(): void {
		if (!this.photo) return;

		this.onSave.emit(this.photo);
		
		this.drawerService.hide('cordova-camera');
	}
}
