import { NgModule } from '@angular/core';

import { NgxAngularCommonsCoreModule } from 'ngx-angularcommons-core';
import { NgxAngularCommonsAppModule } from 'ngx-angularcommons-app';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';

import { CordovaCameraComponent } from './components/cordova-camera/cordova-camera.component';

@NgModule({
	imports: [
			NgxAngularCommonsCoreModule,
			NgxAngularCommonsAppModule,
			NgxWebAppCommonsCoreModule,
			NgxWebAppCommonsAppModule,
			NgxWebAppCommonsMobileModule
	],
	declarations: [ CordovaCameraComponent ],
	exports: [ CordovaCameraComponent ]
})
export class NgxCordovaAngularCommonsCameraModule {}
