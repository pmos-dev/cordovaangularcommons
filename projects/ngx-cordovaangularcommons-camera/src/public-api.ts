/*
 * Public API Surface of ngx-cordovaangularcommons-camera
 */

export * from './lib/components/cordova-camera/cordova-camera.component';

export * from './lib/ngx-cordovaangularcommons-camera.module';
